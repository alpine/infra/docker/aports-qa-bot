FROM alpine:3.13 AS builder
RUN apk add --no-cache go git
RUN git clone https://gitlab.alpinelinux.org/Cogitri/aports-qa-bot
WORKDIR /aports-qa-bot
RUN go build

FROM alpine:3.13
COPY --from=builder /aports-qa-bot/aports-qa-bot /usr/bin/aports-qa-bot
